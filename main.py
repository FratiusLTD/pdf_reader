import os
import sys
import pytesseract
import fitz
from PySide6.QtGui import QImage, QPixmap
from PySide6.QtWidgets import QApplication, QMainWindow, QWidget, QVBoxLayout, QPushButton, QFileDialog, QLabel, QHBoxLayout
from selectable import SelectableLabel
from process import Process
from pdf2image import convert_from_bytes
from io import BytesIO

try:
    from PIL import Image
except ImportError:
    import Image

class MainWindow(QMainWindow):
    def __init__(self):
        super().__init__()

        # Set the title of the main window
        self.setWindowTitle("Main Window")

        # Create a central widget
        self.central_widget = QWidget()
        self.setCentralWidget(self.central_widget)

        # Create a layout for the central widget
        self.layout = QVBoxLayout(self.central_widget)

        # Add additional widgets to the layout
        self.setup_ui()
        
        # Create a Process object
        self.process = Process()

    def setup_ui(self):
        # Create a label to display the selected file path
        self.label = QLabel("Please select a file")
        self.layout.addWidget(self.label)
        self.pdf_label = SelectableLabel()

        # Create a button to trigger the file selection dialog
        self.select_file_button = QPushButton("Select File")
        self.select_file_button.clicked.connect(self.open_file_dialog)
        self.layout.addWidget(self.select_file_button)

        # Create another button for additional actions
        self.process_file_button = QPushButton("Process File ")
        self.layout.addWidget(self.process_file_button)

        self.process_button = QPushButton("Rename File")
        self.process_button.clicked.connect(self.process_selected_area)
        self.layout.addWidget(self.process_button)

    def open_file_dialog(self):
        # Open file dialog and return the selected file path
        file_path, _ = QFileDialog.getOpenFileName(self, "Select File")
        if file_path:
            # File path is selected
            self.label.setText(f"Selected file: {file_path}")
            # Perform processing on the selected file
            self.process_file_button.clicked.connect(self.process.process_file(file_path))
            
        else:
            # No file was selected
            self.label.setText("No file selected")
        
    def open_file_dialog(self):
        file_path, _ = QFileDialog.getOpenFileName(self, "Select File", filter="PDF Files (*.pdf)")
        if file_path:
            self.label.setText(f"Selected file: {file_path}")
            self.display_pdf(file_path)

    def display_pdf(self, file_path):
        # Open the PDF file with PyMuPDF
        self.pdf_document = fitz.open(file_path)
        self.pdf_page = self.pdf_document[0]  # Just take the first page for simplicity
        # Render the page to an image
        pix = self.pdf_page.get_pixmap()
        img = QImage(pix.samples, pix.width, pix.height, pix.stride, QImage.Format_RGB888)
        self.pdf_label.setPixmap(QPixmap.fromImage(img))
        # Add the pdf_label to the layout and show it
        self.layout.addWidget(self.pdf_label)
            
    def process_selected_area(self):
        if self.pdf_label.selection_rect.isNull():
            print("No area selected.")
            return

        if hasattr(self, 'pdf_document') and self.pdf_document:
            # Extract the text from the selected area
            zoom = 1  # Zoom factor for better OCR accuracy
            
            # TODO: save the rectangle coordinates in a global variable to be used to extract the selected area from other files
            rect = fitz.Rect(self.pdf_label.selection_rect.topLeft().x() / zoom,
                            self.pdf_label.selection_rect.topLeft().y() / zoom,
                            self.pdf_label.selection_rect.bottomRight().x() / zoom,
                            self.pdf_label.selection_rect.bottomRight().y() / zoom)

            # Perform OCR using pytesseract
            # First, we need to get the image from the selected area
            # This will return a pixmap
            clip_image = self.pdf_page.get_pixmap(clip=rect)

            clip_image.save(filename = 'temp_img.png', output= "png")
            # Perform OCR using pytesseract
            ocr_text = pytesseract.image_to_string(Image.open('temp_img.png'))

            print(f'ocr_text is {ocr_text}')

            new_filename = self.generate_new_filename(ocr_text)
            self.rename_file(new_filename)
            
    def generate_new_filename(self, text):
    # This is a placeholder for your logic to generate a new filename based on the text
    # For example, you might want to replace spaces with underscores and add a prefix/suffix
        new_filename = text.replace(" ", "_") + "_renamed.pdf"
        return new_filename

    def rename_file(self, new_filename):
        # Assuming the file to be renamed is the currently opened PDF
        if hasattr(self, 'pdf_document') and self.pdf_document:
            new_file_path = os.path.join(os.path.dirname(self.pdf_document.name), new_filename)
            os.rename(self.pdf_document.name, new_file_path)
            # Close the PDF document if it's open
            self.pdf_document.close()
            print(f"File has been renamed to {new_file_path}")
            self.label.setText(f"File has been renamed to: {new_file_path}")

# Create the Qt Application
app = QApplication(sys.argv)

# Create and show the main window
main_window = MainWindow()
main_window.show()

# Run the main Qt loop
sys.exit(app.exec())
