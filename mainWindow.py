import sys
import os
from process import Process
from PySide6.QtWidgets import QApplication, QWidget, QVBoxLayout, QPushButton, QFileDialog, QLabel

class FilePicker(QWidget):
    def __init__(self):
        super().__init__()
        
        self.processing = Process()

        # Set up the layout
        layout = QVBoxLayout()

        # Create a label to display the selected file path
        self.label = QLabel("Please select a file")
        layout.addWidget(self.label)

        # Create a button to trigger the file selection dialog
        self.button = QPushButton("Select File")
        self.button.clicked.connect(self.open_file_dialog)
        layout.addWidget(self.button)

        # Set the layout on the application's window
        self.setLayout(layout)

    def open_file_dialog(self):
        # Open file dialog and return the selected file path
        file_path, _ = QFileDialog.getOpenFileName(self, "Select File")
        if file_path:
            # File path is selected
            self.label.setText(f"Selected file: {file_path}")
            # Perform operations on the selected file
            self.process_file(file_path)
            # get the location of processed file
            # self.label.setText(f"Proccessed file: 
            
            directory = os.path.dirname(file_path)
            file_name = os.path.basename(file_path)
            processed_location = os.path.join(directory, './output', file_name + '.csv')
            
            self.label.setText(f"Proccessed file: {processed_location}")
            # path = '/Users/fratiusltd/pdf_reader/file_list/Ghidul_StepUP_Elite_-_versiune_2022_3.pdf'
            # processed_location = '/Users/fratiusltd/pdf_reader/file_list/./output/Ghidul_StepUP_Elite_-_versiune_2022_3.pdf.csv'
        else:
            # No file was selected
            self.label.setText("No file selected")

    def process_file(self, path):
        # Dummy function to represent processing the file
        # print(f"Processing file: {path}")
        self.processing.process_file(path)

# Create the Qt Application
app = QApplication(sys.argv)

# Create and show the form
file_picker = FilePicker()
file_picker.show()

# Run the main Qt loop
sys.exit(app.exec())
