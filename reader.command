#!/usr/bin/env python
# import libs
try:
    from PIL import Image
except ImportError:
    import Image
import cv2
import pytesseract
import os
import numpy as np
import pandas as pd
import re
from pdf2image import convert_from_bytes

PATH = './file_list'
file_list = os.listdir(PATH)

# Some help functions 
def get_conf(page_gray):
    '''return a average confidence value of OCR result '''
    df = pytesseract.image_to_data(page_gray,output_type='data.frame')
    df.drop(df[df.conf==-1].index.values,inplace=True)
    df.reset_index()
    return df.conf.mean()
  
def deskew(image):
    '''deskew the image'''
    gray = cv2.bitwise_not(image)
    temp_arr = cv2.threshold(gray, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)[1]
    coords = np.column_stack(np.where(temp_arr > 0))
    angle = cv2.minAreaRect(coords)[-1]
    if angle < -45:
        angle = -(90 + angle)
    else:
        angle = -angle
    (h, w) = image.shape[:2]
    center = (w // 2, h // 2)
    M = cv2.getRotationMatrix2D(center, angle, 1.0)
    rotated = cv2.warpAffine(image, M, (w, h), flags=cv2.INTER_CUBIC, borderMode=cv2.BORDER_REPLICATE)
    return rotated
  
'''
Main part of OCR:
pages_df: save eextracted text for each pdf file, index by page
OCR_dic : dict for saving df of each pdf, filename is the key
'''

OCR_dic={} 
for file in file_list:
    print(file)
    # convert pdf into image
    pdf_file = convert_from_bytes(open(os.path.join(PATH,file), 'rb').read())
    # create a df to save each pdf's text
    pages_df = pd.DataFrame(columns=['conf','text'])
    for (i,page) in enumerate(pdf_file) :
        try:
            # transfer image of pdf_file into array
            page_arr = np.asarray(page)
            # transfer into grayscale
            page_arr_gray = cv2.cvtColor(page_arr,cv2.COLOR_BGR2GRAY)
            # deskew the page
            page_deskew = deskew(page_arr_gray)
            # cal confidence value
            page_conf = get_conf(page_deskew)
            # extract string 
            pages_df = pages_df._append({'conf': page_conf,'text': pytesseract.image_to_string(page_deskew)}, ignore_index=True)
        except:
            # if can't extract then give some notes into df
            pages_df = pages_df._append({'conf': -1,'text': 'N/A'}, ignore_index=True)
            continue
        
    # save df into a csv file
    os.makedirs('./output', exist_ok=True)
    pages_df.to_csv('./output/{}.csv'.format(file))
    
    
    # save df into a dict with filename as key        
    OCR_dic[file]=pages_df
    print('{} is done'.format(file))
    # print(OCR_dic[file])
