from PySide6.QtWidgets import QLabel

from PySide6.QtCore import QRect, QPoint, Qt
from PySide6.QtGui import QPainter, QPen
from PySide6.QtWidgets import QLabel

class SelectableLabel(QLabel):
    def __init__(self):
        super().__init__()
        self.selection_rect = QRect()
        self.start_point = QPoint()
        self.end_point = QPoint()
        self.is_selecting = False

    def mousePressEvent(self, event):
        self.start_point = event.pos()
        self.end_point = self.start_point
        self.is_selecting = True
        self.update()

    def mouseMoveEvent(self, event):
        if self.is_selecting:
            self.end_point = event.pos()
            self.update()

    def mouseReleaseEvent(self, event):
        self.end_point = event.pos()
        self.is_selecting = False
        self.update()

    def paintEvent(self, event):
        super().paintEvent(event)
        if not self.start_point.isNull() and not self.end_point.isNull():
            painter = QPainter(self)
            painter.setPen(QPen(Qt.red, 2, Qt.DashLine))
            self.selection_rect = QRect(self.start_point, self.end_point)
            painter.drawRect(self.selection_rect.normalized())
