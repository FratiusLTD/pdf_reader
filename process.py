try:
    from PIL import Image
except ImportError:
    import Image
import cv2
import pytesseract
import os
import numpy as np
import pandas as pd
import re
import fitz
from pdf2image import convert_from_bytes

class Process():
    
    def __init__(self) -> None:
    
        self.OCR_dic={} 
    
    def get_conf(self, page_gray):
        '''return a average confidence value of OCR result '''
        df = pytesseract.image_to_data(page_gray,output_type='data.frame')
        df.drop(df[df.conf==-1].index.values,inplace=True)
        df.reset_index()
        return df.conf.mean()
  
    def deskew(self, image):
        '''deskew the image'''
        gray = cv2.bitwise_not(image)
        temp_arr = cv2.threshold(gray, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)[1]
        coords = np.column_stack(np.where(temp_arr > 0))
        angle = cv2.minAreaRect(coords)[-1]
        if angle < -45:
            angle = -(90 + angle)
        else:
            angle = -angle
        (h, w) = image.shape[:2]
        center = (w // 2, h // 2)
        M = cv2.getRotationMatrix2D(center, angle, 1.0)
        rotated = cv2.warpAffine(image, M, (w, h), flags=cv2.INTER_CUBIC, borderMode=cv2.BORDER_REPLICATE)
        return rotated
    
    '''
    Main part of OCR:
    pages_df: save eextracted text for each pdf file, index by page
    OCR_dic : dict for saving df of each pdf, filename is the key
    '''
    
    
    # PATH = os.path.join(base_dir, './file_list')
    
    def extract_selected_text(self, path):
        # Extract the text from the selected area
        # Convert QRect to the PyMuPDF Rect format
        # Return the extracted text to be used as a title
        zoom = 2  # Zoom factor for better OCR accuracy
        rect = fitz.Rect(self.pdf_label.selection_rect.topLeft().x() / zoom,
                        self.pdf_label.selection_rect.topLeft().y() / zoom,
                        self.pdf_label.selection_rect.bottomRight().x() / zoom,
                        self.pdf_label.selection_rect.bottomRight().y() / zoom)
        # Clip the page to the area of interest
        clip = self.pdf_page.subset(rect)
        # Convert the clipped area to an image
        clip_image = clip.get_pixmap()
        clip_image_bytes = clip_image.tobytes("png")
        # Perform OCR using pytesseract
        ocr_text = pytesseract.image_to_string(clip_image_bytes, lang='eng')
        # Do something with the extracted text, e.g., rename the file
        return ocr_text
    

    def process_file(self, path):
        """
        Pass a path to a file and return a dataframe with the OCR results
        """
        print('Processing file')
        
        directory = os.path.dirname(path)
        file_list = os.listdir(directory)

        for file in file_list:
            # print(f'file is {file}')
            # convert pdf into image
            pdf_file = convert_from_bytes(open(os.path.join(directory,file), 'rb').read())
            # create a df to save each pdf's text
            pages_df = pd.DataFrame(columns=['conf','text'])
            for (i,page) in enumerate(pdf_file) :
                try:
                    # transfer image of pdf_file into array
                    page_arr = np.asarray(page)
                    # transfer into grayscale
                    page_arr_gray = cv2.cvtColor(page_arr,cv2.COLOR_BGR2GRAY)
                    # deskew the page
                    page_deskew = self.deskew(page_arr_gray)
                    # cal confidence value
                    page_conf = self.get_conf(page_deskew)
                    # extract string 
                    pages_df = pages_df._append({'conf': page_conf,'text': pytesseract.image_to_string(page_deskew)}, ignore_index=True)
                except:
                    # if can't extract then give some notes into df
                    pages_df = pages_df._append({'conf': -1,'text': 'N/A'}, ignore_index=True)
                    continue
                
            # save df into a csv file
            os.makedirs('./output', exist_ok=True)
            pages_df.to_csv('./output/{}.csv'.format(self.extract_selected_text(path)))
            print(f'csv file is saved at {os.path.join(directory,"./output/{}.csv".format(file))}')
            
            
            # save df into a dict with filename as key        
            # self.OCR_dic[file]=pages_df
            # print('{} is done'.format(file))
            # print(OCR_dic[file])