This is a program writen in Python 3.12.0 to read the content of a PDF or a picture and export the content in an Excel file

The program should be able to allow the user to select the areas to export.
Every selected area should be put in an Excel column.
Each column/file should have a custom name based on the content of the file/ or the selected area

Also the program should run on MacOS

STEPS:

1 Read the PDF

    1.1 Install homebrew 

    https://www.funkyspacemonkey.com/how-to-install-homebrew-on-m1-macs-running-macos-monterey?utm_content=cmp-true
    /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
    
    1.2 Install Tesseract & additional packages

    https://medium.com/social-impact-analytics/extract-text-from-unsearchable-pdfs-for-data-analysis-using-python-a6a2ca0866dd
    brew install tesseract

    Add languages
    brew install tesseract-lang

    pip install ocrmypdf
    pip install pytesseract
    pip install opencv-python
    pip install pdf2image

    1.3 Additional packages for pdf2image
    https://github.com/Belval/pdf2image
    brew install poppler
    
    1.4 PyMuPdf AKA fitz
    library to perfrom the pdf reading
    pip install pymupdf

2 Export content into Excel 

3 Run the program as app in MacOS

    Required Libraries:
        pyinstaller - https://pyinstaller.org/en/stable/usage.html
        PySide6 - https://doc.qt.io/qtforpython-6/PySide6/QtWidgets/index.html
        # PyQt5 - https://pythonspot.com/pyqt5-file-dialog/


    3.1 pyinstaller
    pyinstaller -n 'reader' --add-data='./file_list/Ghidul_StepUP_Elite_-_versiune_2022_3.pdf:.' reader.py

    https://medium.com/@jackhuang.wz/in-just-two-steps-you-can-turn-a-python-script-into-a-macos-application-installer-6e21bce2ee71

    in order to create an APP  pyinstaller also needs PyQt5 or PySide6 library (both use Qt - very similar)

    3.2 PySide6
    pip install PySide6

    """
    3.2 PyQt5==5.15.0
    i installed the last version 5.15.11 which i'm suspecting it's not compatible with M2 proccessor 
    https://stackoverflow.com/questions/65901162/how-can-i-run-pyqt5-on-my-mac-with-m1chip-ppc64el-architecture

    Running following command to check the libraries compatibility (as ChatGPT4 instructed)
    otool -L /Users/fratiusltd/.pyenv/versions/3.12.0/envs/pdf_reader/lib/python3.12/site-packages/PyQt5/Qt5/lib/QtWidgets.framework/Versions/5/QtWidgets

    --> @rpath/QtWidgets.framework/Versions/5/QtWidgets (compatibility version 5.15.0, current version 5.15.11)
    
    Solution:
        Rosetta 2 terminal to install the required libraries
        I installed everything from the Terminal using brew



env tips

https://fathomtech.io/blog/python-environments-with-pyenv-and-vitualenv/

https://github.com/pyenv/pyenv